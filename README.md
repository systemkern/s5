[![Source Repo](https://img.shields.io/badge/fork%20on-gitlab-important?logo=gitlab)](https://gitlab.com/systemkern/s5)
[![Gitlab Pipelines](https://gitlab.com/systemkern/s5/badges/master/pipeline.svg)](https://gitlab.com/systemkern/s5/-/pipelines)
[![Dockerhub @systemkern](https://img.shields.io/docker/pulls/systemkern/s5)](https://hub.docker.com/r/systemkern/s5)
[![Twitter @systemkern](https://img.shields.io/badge/follow-%40systemkern-blue?logo=twitter)](https://twitter.com/systemkern)
[![LinkedIn @systemkern](https://img.shields.io/badge/contact%20me-%40systemkern-blue?logo=linkedin)](https://linkedin.com/in/systemkern)



Systemkern Simple Static Site Setup (S5)
========================================

Systemkern's Simple Static Site Setup facilitates the deployment of webapps and other projects
by providing image variants for deploying to different kinds of server infrastructure

Every image contains following tools:
* ssh
* an ftp client
* curl
* jq (for formatting json responses)
* zip



Usage
--------------------
To run S5 from a docker container use this command:
```
docker run --rm --tty --volume ${HOME}:/root --volume ${PWD}:/${PWD} registry.gitlab.com/systemkern/s5:latest s5
```


Alternative Usage 2
--------------------
Add the following alias to your `~/.bash_rc` or `~/.bash_profile` (depending on your OS) 

```
#   --rm                       # Automatically remove the container when it exits
#   --tty                      # Allocate a pseudo-TTY
#   --volume ${HOME}:/root	    # Bind mount host os user home directory to container root
#   --volume ${pwd}:/app       # Mount current directory to /app
#   s5:latest                  # Select docker image
alias s5-docker="docker run --name=systemkern-s5-shell-alias-container \
    --rm                        \
    --tty                       \
    --volume ${HOME}:/root      \
    --volume ${PWD}:${PWD}      \
    registry.gitlab.com/systemkern/s5:latest"
```

then execute `s5-docker bash -c 'echo "Hello world from within the s5 docker container'` 


Build Locally 
--------------------
execute `bin/build-locally.sh`


Test Locally 
--------------------
execute `bin/build-locally.sh && run-in-s5-docker s5` 

