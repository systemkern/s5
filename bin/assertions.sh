#!/bin/sh

assertEqual() {
  if [ "$1" != "$2" ]; then
    echo "ERROR - ASSERTION FAILED: expected <""$1""> but was <""$2"">"
    exit 1
  fi
}

assertDirectoryContentEqual() {
  find "$1" -type f \
    -exec echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -" \; \
    -exec basename -- {} \
    -exec cat {} \; \
    >expected

  find "$2" -type f \
    -exec echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -" \; \
    -exec basename -- {} \
    -exec cat {} \; \
    >actual

  set +e
  diff expected actual > diff.out
  set -e

  # shellcheck disable=SC2046
  # regex matches any character or any number case insensitive
  # if the count is > 0 there is an error and we print it out
  if [ $(grep "+++" -c < diff.out) -ne 0 ]; then
    echo "ERROR - ASSERTION FAILED: expected diff to be empty but was:"
    cat diff.out
    echo "------------------------------------------------------------"
    echo "expected: $1"
    find "$1" -type f \
      -exec echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -" \; \
      -exec echo {} \; \
      -exec cat {} \; \

    echo "------------------------------------------------------------"
    echo "actual: $2"
    find "$2" -type f \
      -exec echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -" \; \
      -exec echo {} \; \
      -exec cat {} \; \

    exit 1
  fi
}
