#!/usr/bin/env bash -e
# shellcheck disable=SC2096

# prepare environment, see ../gitlab-ci.yml
export IMAGE_NAME="s5"
export DOCKER_REGISTRY="registry.gitlab.com"
export DOCKER_ORGANISATION="systemkern"

export TAG="nightly"
export IMAGE_PATH="${DOCKER_REGISTRY}/${DOCKER_ORGANISATION}/${IMAGE_NAME}:${TAG}"

echo "IMAGE_PATH: $IMAGE_PATH"

# build with default name to not spam developer machines
# for faster developer builds build without --pull
docker build --quiet --tag s5:nightly        --tag "$IMAGE_PATH" -f Dockerfile .
docker build --quiet --tag s5:nightly-python --tag "$IMAGE_PATH-python" -f Dockerfile-python .
docker build --quiet --tag s5:nightly-aws    --tag "$IMAGE_PATH-aws" -f Dockerfile-aws .

# test run image (mainly for log output)
#docker run --rm --name="systemkern-$IMAGE_NAME-container" --tty "$IMAGE_NAME"
