#!/bin/sh
# shellcheck disable=SC2112
set -e # immediately exit on error

###############################################################################
# set environment variables
export TESTDIR="$PWD/src/test"
export PATH="${PWD}/src/main:${PATH}"
echo "using path: ${PATH}"
alias markdown="../../external/markdown"
###############################################################################
# include assertions
source bin/assertions.sh
###############################################################################
# define testing functions

function startTest() {
  prepareTest "$1"
  cd "$CURDIR"  # switch to test directory
  RES=$(s5)     # run test
  cd "$TESTDIR" # switch back to application root
  printResults $1
  echo "$RES" # return stdout
}

function prepareTest() {
  # shellcheck disable=SC2034
  CURDIR="$TESTDIR/$1"
  echo "======================================================================="
  echo "======================================================================="
  echo "Starting test: $1"
  echo "current dir is: $CURDIR"
  ls -al "$CURDIR"
}

function printResults() {
  echo "======================================================================="
  find "$CURDIR" -type f \
    -exec echo "- - - - - - - - - - - - - - - - - - - - - - - - - - -" \; \
    -exec echo {} \; \
    -exec cat {} \;
  echo "======================================================================="
}

###############################################################################

prepareTest "no-index-html"
assertEqual "$(s5)" "index.html does not exist"

startTest "simple-page"
assertDirectoryContentEqual "$CURDIR-expected" "$CURDIR"

startTest "nested-tags"
assertDirectoryContentEqual "$CURDIR-expected" "$CURDIR"

startTest "markdown-rendering"
assertDirectoryContentEqual "$CURDIR-expected" "$CURDIR"


###############################################################################
echo "============================================================="
echo "All tests completed successfully"
exit 0
